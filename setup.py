#!/usr/bin/env python

from setuptools import setup, find_packages
from codecs import open
import os.path
import sys

setup(
    name="portal-tools",
    version="0.2",
    author="Leigh Stoller",
    author_email="stoller@flux.utah.edu",
    url="https://gitlab.flux.utah.edu/stoller/portal-tools",
    description="Programmatic interface to the Cloudlab portal",
    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: System Administrators",
        "Operating System :: POSIX :: Linux",
        "Topic :: Internet",
        "Topic :: System :: Networking",
        "Topic :: System :: Networking :: Monitoring",
        "Topic :: System :: Systems Administration",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7"
    ],
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    entry_points={
      "console_scripts": [
        "script_wrapper.py=emulab_sslxmlrpc.client.__main__:main",
        "startExperiment=emulab_sslxmlrpc.client.__main__:startExperiment",
        "modifyExperiment=emulab_sslxmlrpc.client.__main__:modifyExperiment",
        "terminateExperiment=emulab_sslxmlrpc.client.__main__:terminateExperiment",
        "experimentStatus=emulab_sslxmlrpc.client.__main__:experimentStatus",
        "extendExperiment=emulab_sslxmlrpc.client.__main__:extendExperiment",
        "experimentManifests=emulab_sslxmlrpc.client.__main__:experimentManifests",
        "experimentReboot=emulab_sslxmlrpc.client.__main__:experimentReboot",
        "createReservation=emulab_sslxmlrpc.client.__main__:createReservation",
        "reservationStatus=emulab_sslxmlrpc.client.__main__:reservationStatus",
        "deleteReservation=emulab_sslxmlrpc.client.__main__:deleteReservation",
        ],
    }
)
