#!/bin/sh
#
# Install the portal tools package
#
DIRNAME=`dirname $0`

sudo apt-get update
if [ $? -ne 0 ]; then
    echo 'apt-get update failed'
    exit 1
fi

sudo apt-get -y install python-setuptools
if [ $? -ne 0 ]; then
    echo 'apt-get install setuptools failed'
    exit 1
fi

cd $DIRNAME
python setup.py install --user

if [ $? -ne 0 ]; then
    echo 'setup.py install failed'
    exit 1
fi

exit 0
