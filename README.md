#### Using this profile

This repository based [profile](https://www.cloudlab.us/show-profile.php?uuid=bdca59db-aa6a-11e9-8677-e4434b2381fc)
sets up a small environment on a node, that allows you to start, monitor,
and terminate Portal based experiments from the command line. In addition
to the command line tools described below, there is also a module based
API that you can included in your python programs. See 
[src/example.py](src/example.py) for a simple demonstration of how 
you can do that (more on this below).

The main requirement for using the command line tool is your Portal SSL key
and certificate, which you can get from the Portal web interface. Click on
your name in the upper right, and choose the **Download Credentials** menu
option. You will get a PEM file, with your encrypted key and certificate.
The passphrase for the key is the same as your Portal login password. Once
your node is running, log in and:

	node> mkdir .ssl
	node> chmod 700 .ssl

Then scp the PEM file to *.ssl/emulab.pem*. Since the key is encrypted, you
will be asked to provide the passphrase. You can decrypt the key if you
like and store that in the PEM file (e.g.,
`( openssl rsa -in cloudlab.pem ; openssl x509 -in cloudlab.pem ) > cloudlab-decrypted.pem`).
But please, *do not store your key in a public repository!*

The next thing you need to do is add your local python bin directory to
your path.  For *sh*, this would be:

	node> PATH=$HOME/.local/bin:$PATH

#### Starting an Experiment

The command line program to start an experiment is:

	node> startExperiment 
	Usage: startExperiment <options> 
	where:
	 -w           - Wait mode (wait for experiment to start)
	 -a urn       - Override default aggregate URN
	 --project    - pid[,gid]: project[,group] for new experiment
	 --name       - Optional pithy name for experiment
	 --duration   - Number of hours for initial expiration
	 --start      - Schedule experiment to start at (unix) time
	 --stop       - Schedule experiment to stop at (unix) time
	 --paramset   - uid,name of a parameter set to apply
     --bindings   - file containting json string of bindings to apply
	 --refspec    - refspec[:hash] of a repo based profile to use
	 --site       - Bind sites used in the profile
	 profile      - Either UUID or pid,name

For example, to start the example profile
[single-pc](https://www.cloudlab.us/p/PortalProfiles/single-pc)
(which is in the *PortalProfiles* project), in one of your own projects:

	node> startExperiment --project myproject --name myexp PortalProfiles,single-pc

This might take anywhere from 10 seconds to a minute before it
returns, and once it does, your experiment is being created, which
might take another few minutes, or a really long time, depending on
the profile.

#### Parameterized Profiles and Bindings

If the profile you are instantiating is parameterized, it is very handy to
be able to specify a set of bindings to apply, to override the profile
defaults. One way to do this is with *Parameter Sets*, which are created
via the web interface when you instantiate a profile. After the
parameterize step, you will have the option to save the current bindings so
that you can reuse them quickly, the next time you instantiate the profile.
See the web interface for more details.

Alternatively, you can use the **--bindings** argument to provide a json
string of key/value pairs. For example:

    {
	    "nodeid" : "pc11",
        "image"  : "UBUNTU18-64-STD" 
	}
	
Just send this as a string:

	--bindings='{"nodeid" : "pc11",	"image" : "UBUNTU18-64-STD"}'

#### Experiment Status

After you start your experiment, you will want to check its status to
know when it is *ready* (or failed):

	node> experimentStatus myproject,myexp
	Status: provisioning
	node> experimentStatus myproject,myexp
	Status: ready

Please, **no polling loops with a short timeout**!  Also note that you
will receive email when your experiment is ready (or fails).

Alternatively, you can request a much more useful JSON result using
the **-j** option.

	node> experimentStatus -j myproject,myexp
    {
	    "status" : "ready",
	    "uuid"   : "659177fe-de4c-11ea-b1eb-e4434b2381fc",
		"expires": "2020-10-29T16:47:35Z",
        "execute_status" : {
			"failed"   : 0,
			"running"  : 0,
			"finished" : 1,
			"total"    : 1
		},
	}

#### Experiment Manifests

Once the experiment is ready, you can ask for the manifests(s). 

	node> experimentManifests myproject,myexp
	
This will return a json encoded array. The key is the URN of each aggregate
and the value is the manifest. You will need to json decode the array, and
then XML decode the manifest.
	
#### Terminating an Experiment

To terminate an experiment:

	node> terminateExperiment myproject,myexp

After a little while:

	node> experimentStatus myproject,myexp
	*** manage_instance:
	    No such instance myproject,myexp
		
#### Extending an Experiment

Experiments can be extended in hour units. In general, the first week or
two will be granted automatically. Beyond that, an administrator will need
to approve the request. As with the web interface, if your request requires
administrator intervention, you will receive email when it is approved (or
rejected). If your request is granted, the exit value will be zero. A
non-zero exit means some or none of your request was granted, use the
`experimentStatus` call to get the current expiration so you can determine
how much was granted. 

	node> extendExperiment -m 'I need them' myproject,myexp 24
	
A word about the **-m** option; you must supply an explanation for your
request, which can be done on the command line, but it is more likely that
you will want to use a text file to provide more detail. Use the **-f**
option instead, providing the filename. Note that the more detail you
provide, the more likely an administrator will approve your request.

#### Reboot nodes in an Experiment

You can reboot and power cycle (when needed) nodes in an experiment.

	node> experimentReboot myproject,myproject node1 node2 ...
	
Add the **-f** option to power cycle instead, but this should be 
considered a last resort option.

#### Modify an Experiment

The command line program to start an experiment is:

	node> modifyExperiment 
	Usage: modifyExperiment <options>
	where:
	 -a urn       - Override default aggregate URN
     --bindings   - file containting json string of bindings to apply
	 experiment   - Either UUID or pid,name

You can modify an existing experiment that is in the *ready* state.
Only parameterized experiments can be modified, by submitting a new
set of bindings to apply. For example, suppose you have a profile that
took a single argument, the number of nodes. And you had started the
experiment by supplying this binding string:

	--bindings='{"count" : "1"}'
	
You can increase the number of nodes with:

	node> modifyExperiment --bindings='{"count" : "2"}' myproject,myexp

Note that this interface is still under development, please let us
us know if you have problems. 

#### Parameter Sets

*Parameter sets* are named sets of key/value pairs, that define a set of
selections for a parameterized profile. In the Web UI, parameter sets are
created using the *Save Parameters* button on the status page of a running
experiment, or on the history page of a terminated experiment. More
information can be found in the Web UI, but for now lets say that you have
a parameter set defined for one of your profiles and that it is called
*myparamset*. To start an experiment using that profile and parameter set:

	node> startExperiment --project myproject --name myexp --paramset myuid,myparamset PortalProfiles,select-os

Note that *myuid* is your login id, but this argument will likely be
removed or made optional in a future release.

#### Reservations

You can submit a reservation request for resources in a similar manner 
to how resource reservations are created via the web interface. The 
difference is that instead of specifying the resources directly, a
profile and optional bindings are specified. After the optional bindings
are applied, the result should be a *bound* set of resources to reserve.
In other words, the clusters for every resource are now known, and the
server can create a reservation from that information. If the reservation
can be immediately auto approved, the exit (return) value is zero. If the
reservation needs administrator approval, the exit (return) value is non
zero. 

The command line program to submit a reservation is:

    node> createReservation 
    Usage: createReservation <options>  [--site 'site:1=aggregate ...'] --end <when> <profile>
    where:
    -a urn       - Override default aggregate URN
    -n           - Check only mode, do not submit
    --project    - pid[,gid]: project[,group] for new experiment
    --start      - Reservation start time (unix time or datetime)
    --end        - Reservation end time (unix time or datetime)
	--duration   - HOURS instead of start/end, we will search for a slot
    --paramset   - uid,name of a parameter set to apply
    --bindings   - json string of bindings to apply to parameters
    --bindingsfile - json file of bindings to apply to parameters
    --refspec    - refspec[:hash] of a repo based profile to use
    --site       - Bind sites used in the profile
    profile       - Either UUID or pid,name

You must supply an *end* time (unix timestamp or GMT date string), the
project to create the reservation in, and the profile to use to determine
the resources. For a parameterized profile, the easiest way to supply
bindings is with a parameter set, but you can also provide a json string
(or a file) of bindings. For example:

    node> createReservation --end '2023-04-13T17:11:00Z' --paramset myuid,myparamset --project myproject testbed,spectrum-test 

The result is a json object with reservations details. For example:

    {
      "cluster_results" : {
        "uuid"     : "579d45c9-ae3b-11ee-9f39-e4434b2381fc",
        "approved" : 2,
	    "clusters" : {
	      "8cb6c75b-ae3f-11ee-9f39-e4434b2381fc" : {
		    "approved" : 1,
			"cluster"  : "urn:publicid:IDN+emulab.net+authority+cm",
			"count"    : 1,
			"type"     : "d430",
            "uuid"     : "8cb6c75b-ae3f-11ee-9f39-e4434b2381fc"
          },
          "8cb7c150-ae3f-11ee-9f39-e4434b2381fc" : {
            "approved" : 1,
             "cluster" : "urn:publicid:IDN+wisc.cloudlab.us+authority+cm",
             "count"   : 1,
             "type"    : "c220g5",
             "uuid"    : "8cb7c150-ae3f-11ee-9f39-e4434b2381fc"
          },
        },
      },
      "errors" : 0
    }
	
You can ask for status of a reservation, say if it needed approval:

    node> reservationStatus 579d45c9-ae3b-11ee-9f39-e4434b2381fc
	
which will return results similar to above. You can also delete a
reservation:

    node> deleteReservation 579d45c9-ae3b-11ee-9f39-e4434b2381fc
	
Reservation modification is not supported at this time.

#### Installing on your desktop

You can install the tools on your desktop using the provided ```setup.py``` 
script. You may need to install the python (or python3) ```setuptools```
package first. See ```install.sh``` for an example of how this is done
on Ubuntu.

    node> python setup.py install --user

which will install the tools in your ```~/.local``` directory, as described
in first section of this document.

If you want to install the tools for all users:

    node> sudo python setup.py install

#### Using the API

In addition to the command line interface, the API is a module you can load
into your python programs. The same entry points described above are
exported from the module. You will also need to import the ```xmlrpc```
module, as demonstrated in [src/example.py](src/example.py). 
A brief fragment follows:


	import emulab_sslxmlrpc.client.api as api
	import emulab_sslxmlrpc.xmlrpc as xmlrpc
 
    # Create the RPC server object.
    config = {
        "debug"       : 0,
        "impotent"    : 0,
        "verify"      : 0,
        "certificate" : "/path/to/your/portal/certificate.pem",
    }
    server = xmlrpc.EmulabXMLRPC(config)
	
	#
	# Request status of a running experiment
	#
    params = {
        "experiment" : "testbed,myexp",
    }
    (exitval,response) = api.experimentStatus(rpc, params).apply();
	
	#
	# Ask for the manifest
	#
    (exitval,response) = api.experimentManifest(rpc, params).apply();
	print(response)
	
More detailed information is in the [API description](API.md).
